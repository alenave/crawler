package model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class News implements Comparable<News> {
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getSourceUrl() {
		return sourceUrl;
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	public String getPublishedAt() {
		return publishedAt;
	}

	public void setPublishedAt(String publishedAt) {
		this.publishedAt = publishedAt;
	}

	private String title;
	private String description;
	private String image;
	private String sourceUrl;
	private String publishedAt;
	private String sourceName;
	private String sortPublishAt;

	public String getSortPublishAt() {
		return sortPublishAt;
	}

	public void setSortPublishAt(String sortPublishAt) {
		this.sortPublishAt = sortPublishAt;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public int compareTo(News feedItem) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date1 = null;
		Date date2 = null;
		try {
			date1 = formatter.parse(feedItem.getPublishedAt());
			date2 = formatter.parse(publishedAt);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (date1.compareTo(date2) >= 0) {
			return 1;
		}
		return -1;
	}

}