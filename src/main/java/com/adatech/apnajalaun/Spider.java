package com.adatech.apnajalaun;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import jagran.SpiderLeg;
import jalaun_times.JalaunTimesSpiderLeg;
import model.News;
import ujala.UjalaSpiderLeg;
import utils.Config;
import utils.JsonParser;
import utils.Request;

public class Spider {
	public static Map<String, News> feedFinalList = new HashMap<String, News>();
	public static Map<String, News> newsFinalList = new HashMap<String, News>();
	public static List<News> newsList = new ArrayList<News>();
	private static List<News> adList = new ArrayList<News>();
	public static int feedCounter = 1;
	public static int newsCounter = 1;
	static SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	static Date date;

	public static void main(String[] args) throws JSONException, IOException, InterruptedException {
		new JalaunTimesSpiderLeg().crawl();
		new SpiderLeg().crawl(Config.MAXIMUM_PAGE_TO_BE_FETCHED);
//		new UjalaSpiderLeg().crawl();
		JSONObject localNewsObject = new Request().get(Config.FIREBASE_URL + Config.LOCAL_NEWS_END_POINT);
		if (localNewsObject != null) {
			newsList.addAll(new JsonParser().getLocalNews(localNewsObject));
		}
		Collections.sort(newsList);
		JSONObject adObject = new Request().get(Config.FIREBASE_URL + Config.AD_END_POINT);
		if (adObject != null) {
			adList.addAll(new JsonParser().getAds(adObject));
			Collections.sort(adList);
		}
		for (int i = 0, j = 0; i < newsList.size(); i++, j++) {
			date = new Date();
			Thread.sleep(1);
			newsList.get(i).setSortPublishAt(output.format(date));
			feedFinalList.put(String.valueOf(Spider.feedCounter++), newsList.get(i));
			if (adObject != null && j < adList.size()) {
				date = new Date();
				Thread.sleep(2);
				adList.get(i).setSortPublishAt(output.format(date));
				feedFinalList.put(String.valueOf(Spider.feedCounter++), adList.get(j));
			}
		}
		new Request().post(feedFinalList, Config.FEEDS_END_POINT);
	}

}