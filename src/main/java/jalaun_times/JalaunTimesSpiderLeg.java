package jalaun_times;

import utils.Config;
import utils.JsonParser;
import utils.Request;

public class JalaunTimesSpiderLeg {
	public void crawl() {
		new JsonParser().getJalaunTimesNews(new Request().get(Config.BASE_URL_JALAUN_TIMES));
	}
}
