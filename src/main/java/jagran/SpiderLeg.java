package jagran;

import java.util.LinkedList;
import java.util.List;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import utils.Config;

public class SpiderLeg
{
    // We'll use a fake USER_AGENT so the web server thinks the robot is a normal web browser.
    private static final String USER_AGENT =
            "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0 AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1 Opera/9.80";
    private List<String> links = new LinkedList<String>();
    private Document htmlDocument;


    /**
     * This performs all the work. It makes an HTTP request, checks the response, and then gathers
     * up all the links on the page.
     * @param url
     *            - The URL to visit
     */
    public int crawl(int pageNumber)
    {
    	int num=-1;
        try
        {
        	
        	for(int pageIndex = 1; pageIndex <= pageNumber; pageIndex++) {
        	String page = pageIndex + ".html";
            Connection connection = Jsoup.connect(Config.BASE_URL_JAGRAN + Config.PAGE_URL_JAGRAN + page).userAgent(USER_AGENT);
            Document htmlDocument = connection.timeout(100000).get();
            Elements linksOnPage;
            this.htmlDocument = htmlDocument;
            if(connection.response().statusCode() == 200) // 200 is the HTTP OK status code
                                                          // indicating that everything is great.
            {
                linksOnPage = this.htmlDocument.getElementsByClass("topicList");
                for(Element article : linksOnPage) {
                	Elements local = article.getElementsByTag("li");
                	for(Element localArticle : local) {
                		Element href = localArticle.getElementsByTag("a").first();
//                		System.out.println(href.attr("href"));
                		setLinks(href.attr("href").toString());
                	}
                }
            }
       }
        	new SpiderCrawler().crawler(getLinks());
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        	System.out.println(e);
        }
        
      return num;
    }

    public List<String> getLinks()
    {

        return links;
    }
    
    public void setLinks(String href) {
    	links.add(href);
    }

}