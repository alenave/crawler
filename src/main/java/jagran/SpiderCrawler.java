package jagran;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.adatech.apnajalaun.Spider;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseCredentials;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import model.News;

import org.jsoup.nodes.Element;

import utils.Config;
import utils.Operations;

public class SpiderCrawler {

	private Set<String> pagesVisited = new HashSet<String>();
	private List<String> pagesToVisit = new LinkedList<String>();

	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0 AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1 Opera/9.80";

	private void connections(String url) throws IOException, JSONException {
		Document htmlDocument = null;
		Connection.Response response;
		response = Jsoup.connect(Config.BASE_URL_JAGRAN + url).userAgent(USER_AGENT).timeout(50000).execute();
		System.out.println(Config.BASE_URL_JAGRAN + url);
		if (response.statusCode() == 200) {
			News news = new News();
			String source = Config.BASE_URL_JAGRAN + url;
			news.setSourceUrl(source);
			htmlDocument = response.parse();
			String publish_date = htmlDocument.select("meta[property=article:published_date]").get(0).attr("content");
			news.setPublishedAt(Operations.dateTimeFormatChangeWithT(publish_date));
			// news.setPrevNews(Config.BASE_URL_JAGRAN +
			// htmlDocument.getElementsByClass("btprev").attr("href"));
			// news.setNextNews(Config.BASE_URL_JAGRAN +
			// htmlDocument.getElementsByClass("btnext").attr("href"));
			Elements articleDetails = htmlDocument.getElementsByClass("ls-canvas");
			for (Element article : articleDetails) {
				Elements headlineElements = article.getElementsByClass("articleHd");
				for (Element headLine : headlineElements) {
					news.setTitle(headLine.getElementsByTag("h1").text());
				}
				if (article.getElementById("jagran_image_id") != null) {
					Element image = article.getElementById("jagran_image_id");
					news.setImage(image.getElementsByTag("img").attr("src"));
				}
				Elements details = article.getElementsByClass("articleBody");
				String content = "";
				for (Element element : details) {
					content = element.getElementsByTag("p").text();
				}
				news.setDescription(content);
				news.setSourceName("Dainik Jagran");
			}
			Spider.newsList.add(news);
			System.out.println(news);
		} else {
			System.out.println("Error aa gai");
		}
	}

	public void crawler(List<String> list) throws IOException, JSONException {
		this.pagesToVisit.addAll(list);
		while (this.pagesToVisit.size() != 0) {
			String currentUrl = null;
			if (this.pagesToVisit.isEmpty()) {
				currentUrl = list.get(0);
				connections(currentUrl);
				this.pagesVisited.add(currentUrl);
			} else {
				currentUrl = this.nextUrl();
				connections(currentUrl);
			}
			System.out.println(this.pagesVisited.size() + "  " + (this.pagesVisited.size() < pagesToVisit.size()) + " "
					+ this.pagesToVisit.size());
		}
	}

	/**
	 * Returns the next URL to visit (in the order that they were found). We
	 * also do a check to make sure this method doesn't return a URL that has
	 * already been visited.
	 * 
	 * @return
	 */
	String nextUrl() {
		String nextUrl;
		do {
			nextUrl = this.pagesToVisit.remove(0);

		} while (this.pagesVisited.contains(nextUrl));
		this.pagesVisited.add(nextUrl);
		return nextUrl;
	}

	public void setData(Map<String, News> allNews) {
		System.out.println("print");
		FileInputStream serviceAccount = null;
		try {
			serviceAccount = new FileInputStream("./lib/service_account.json");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		FirebaseOptions options = new FirebaseOptions.Builder()
				.setCredential(FirebaseCredentials.fromCertificate(serviceAccount)).setDatabaseUrl(Config.FIREBASE_URL)
				.build();
		if (FirebaseApp.getInstance() == null) {
			FirebaseApp.initializeApp(options);
		}
		final FirebaseDatabase database = FirebaseDatabase.getInstance();
		DatabaseReference dbref = database.getReference("list");
		System.out.println(dbref);
		DatabaseReference usersRef = dbref.child("news");
		usersRef.setValue(allNews);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}