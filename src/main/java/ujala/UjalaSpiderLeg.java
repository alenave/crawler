package ujala;

import java.util.LinkedList;
import java.util.List;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import utils.Config;

public class UjalaSpiderLeg {
	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0 AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1 Opera/9.80";
	private List<String> links = new LinkedList<String>();
	private Document htmlDocument;

	public void crawl() {
		try {
			Connection connection = Jsoup.connect(Config.BASE_URL_UJALA).userAgent(USER_AGENT);
			Document htmlDocument = connection.timeout(100000).get();
			Elements linksOnPage;
			this.htmlDocument = htmlDocument;
			if (connection.response().statusCode() == 200) // 200 is the HTTP OK
															// status code
															// indicating that
															// everything is
															// great.
			{
				linksOnPage = this.htmlDocument.getElementsByClass("bigNws");
				for (Element article : linksOnPage) {
					Elements local = article.getElementsByTag("a");
					setLinks(local.attr("href").toString());
				}
				linksOnPage = this.htmlDocument.getElementsByClass("bx2");
				for (Element article : linksOnPage) {
					Elements local = article.getElementsByTag("h3");
					for (Element url : local) {
						setLinks(url.getElementsByTag("a").attr("href").toString());
					}

				}
			}
//			System.out.println(getLinks());
			new UjalaSpiderCrawler().crawler(getLinks());
		} catch (

		Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
	}

	public List<String> getLinks() {

		return links;
	}

	public void setLinks(String href) {
		links.add(href);
	}
}
