package ujala;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.json.JSONException;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.adatech.apnajalaun.Spider;

import model.News;
import utils.Config;
import utils.Operations;

public class UjalaSpiderCrawler {

	private Set<String> pagesVisited = new HashSet<String>();
	private List<String> pagesToVisit = new LinkedList<String>();

	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0 AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1 Opera/9.80";

	private void connections(String url) throws IOException, JSONException {
		Document htmlDocument = null;
		Connection.Response response;
		if (!url.contains("/video/")) {
			response = Jsoup.connect(Config.BASE_URL_UJALA + url).userAgent(USER_AGENT).timeout(50000).execute();
			System.out.println(Config.BASE_URL_UJALA + url);
			if (response.statusCode() == 200) {
				News news = new News();
				String source = Config.BASE_URL_UJALA + url;
				news.setSourceUrl(source);
				htmlDocument = response.parse();
				String publish_date = htmlDocument.getElementsByTag("span").attr("datetime");
				if (!publish_date.equals("")) {
					news.setPublishedAt(Operations.dateTimeFormatChangeWithDayName(publish_date));
				} else {
					Date toPrint = new Date();
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					format.setTimeZone(TimeZone.getTimeZone("IST"));
					news.setPublishedAt(format.format(toPrint));
					System.out.println(format.format(toPrint));
				}
				Elements articleDetails = htmlDocument.getElementsByClass("art-lft");
				for (Element article : articleDetails) {
					String headline = article.getElementsByTag("h1").attr("title");
					news.setTitle(headline);
					String image = "http:" + article.getElementsByTag("img").attr("src");
					news.setImage(image);
					Elements details = article.getElementsByClass("desc");
					String content = "";
					for (Element element : details) {
						content += element.text();
					}
					news.setDescription(content);
					news.setSourceName("Amar Ujala");
				}

				Spider.newsList.add(news);
			} else {
				System.out.println("Error aa gai");
			}
		}
	}

	public void crawler(List<String> list) throws IOException, JSONException {
		this.pagesToVisit.addAll(list);
		while (this.pagesToVisit.size() != 0) {
			String currentUrl = null;
			if (this.pagesToVisit.isEmpty()) {
				currentUrl = list.get(0);
				connections(currentUrl);
				this.pagesVisited.add(currentUrl);
			} else {
				currentUrl = this.nextUrl();
				connections(currentUrl);
			}
			System.out.println(this.pagesVisited.size() + "  " + (this.pagesVisited.size() < pagesToVisit.size()) + " "
					+ this.pagesToVisit.size());
		}
	}

	String nextUrl() {
		String nextUrl;
		do {
			nextUrl = this.pagesToVisit.remove(0);

		} while (this.pagesVisited.contains(nextUrl));
		this.pagesVisited.add(nextUrl);
		return nextUrl;
	}
}
