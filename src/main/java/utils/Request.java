package utils;

import java.io.BufferedReader;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
import java.util.Map;

import org.json.JSONObject;
import org.json.JSONTokener;

import com.firebase.client.Firebase;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseCredentials;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import model.News;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;

public class Request {
	public void post(Map<String, News> allNews, String endPoint) {
		InputStream input = null;
		try {
			input = Request.class.getResourceAsStream("/service_account.json");
		} catch (Exception e) {
			e.printStackTrace();
		}
		FirebaseOptions options = new FirebaseOptions.Builder()
				.setCredential(FirebaseCredentials.fromCertificate(input))
				.setDatabaseUrl(Config.FIREBASE_URL)
				.build();
		if(!(FirebaseApp.getApps().size() > 0)) {
			FirebaseApp.initializeApp(options);
		}
			
		final FirebaseDatabase database = FirebaseDatabase.getInstance();
		DatabaseReference dbref = database.getReference("list");
		DatabaseReference usersRef = dbref.child(endPoint);
		usersRef.setValue(allNews);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if(FirebaseDatabase.getInstance() != null)
	    {
			System.out.println("i m done " + endPoint);
	        Firebase.goOffline();
	    }
	}
	
	public JSONObject get(String urlString) {
        HttpURLConnection con = null;
        JSONObject jo = null;
        try {
            System.out.println(urlString);
            URL url = new URL(urlString);
            if (urlString.startsWith("https")) {
                con =  (HttpURLConnection) url.openConnection();
            } else {
                con = (HttpURLConnection) url.openConnection();
            }
            con.setRequestMethod("GET");
            con.setConnectTimeout(5000);
            con.setReadTimeout(30000);
//            con.setRequestProperty("Content-Type","application/json");
//            con.setRequestProperty("Accept","application/json");
            int HttpResult = con.getResponseCode();
            String msg = "";
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                StringBuilder resp = new StringBuilder();
                String respline = "";
                while ((respline = in.readLine()) != null) {
                    resp.append(respline);
                }
                msg = resp.toString();
                in.close();
//                System.out.println("resp:" + msg);
                JSONObject json = null;
                if(msg != null && !msg.equalsIgnoreCase("null")) {
                	json = (JSONObject) new JSONTokener(msg).nextValue();
                }
                if (json != null) {
                    jo = json;
                } else {
                    jo = null;
                }
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            if (jo == null) {
                jo = new JSONObject();
                try {
                    jo.put("success", false);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        } catch (ConnectException e) {
            e.printStackTrace();
            if (jo == null) {
                jo = new JSONObject();
                try {
                    jo.put("success", false);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jo;
    }
	
}
