package utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.text.BadLocationException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.adatech.apnajalaun.Spider;

import model.News;

public class JsonParser {
	private News currItem;

	public List<News> getAds(JSONObject jo) throws IOException, JSONException {
		List<News> adList = new ArrayList<News>();
		if (jo != null) {
			try {
				Iterator<?> keys = jo.keys();
				while (keys.hasNext()) {
					String key = (String) keys.next();
					JSONObject jsonObject = (JSONObject) jo.get(key);
					if (jo.get(key) instanceof JSONObject) {
						currItem = new News();
						if (jsonObject.has("title")) {
							currItem.setTitle(jsonObject.getString("title"));
						}
						if (jsonObject.has("description")) {
							currItem.setDescription(jsonObject.getString("description"));
						}
						if (jsonObject.has("image")) {
							currItem.setImage(jsonObject.getString("image"));
						} else {
							currItem.setImage(null);
						}
						if (jsonObject.has("sourceUrl")) {
							currItem.setSourceUrl(jsonObject.getString("sourceUrl"));
						}

						if (jsonObject.has("sourceName")) {
							currItem.setSourceName(jsonObject.getString("sourceName"));
						}

						if (jsonObject.has("publishedAt")) {
							currItem.setPublishedAt(jsonObject.getString("publishedAt"));
						}
						adList.add(currItem);
					}
				}
				return adList;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private News news;

	public void getJalaunTimesNews(JSONObject jo) {
		if (jo != null) {
			try {
				if (jo.has("posts")) {
					JSONArray jsonArray = jo.getJSONArray("posts");
					for (int postIndex = 0; postIndex < jsonArray.length(); postIndex++) {
						news = new News();
						JSONObject post = (JSONObject) jsonArray.get(postIndex);
						if (post.has("title")) {
							news.setTitle(post.getString("title"));
						}

						if (post.has("content")) {
							setImageAndDescription(post.getString("content"));
						}

						if (post.has("date")) {
							news.setPublishedAt(Operations.dateTimeFormatChangeWithT(post.getString("date")));
							System.out.println(news.getPublishedAt());
						}

						if (post.has("short_URL")) {
							news.setSourceUrl(post.getString("short_URL"));
							news.setSourceName("Jalaun Times");
							System.out.println(postIndex + 1 + " " + news.getSourceUrl());
						}
						Spider.newsList.add(news);
					}
				}
			} catch (Exception e) {

			}
		}
	}

	public List<News> getLocalNews(JSONObject jo) throws IOException, JSONException {
		News currItem = null;
		List<News> feedList = new ArrayList<News>();
		if (jo != null) {
			try {
				Iterator<?> keys = jo.keys();
				while (keys.hasNext()) {
					String key = (String) keys.next();
					JSONObject jsonObject = (JSONObject) jo.get(key);
					if (jo.get(key) instanceof JSONObject) {
						currItem = new News();
						if (jsonObject.has("title")) {
							currItem.setTitle(jsonObject.getString("title"));
						}
						if (jsonObject.has("description")) {
							currItem.setDescription(jsonObject.getString("description"));
						}
						if (jsonObject.has("image")) {
							currItem.setImage(jsonObject.getString("image"));
						}
						if (jsonObject.has("sourceUrl")) {
							currItem.setSourceUrl(jsonObject.getString("sourceUrl"));
						}

						if (jsonObject.has("sourceName")) {
							currItem.setSourceName(jsonObject.getString("sourceName"));
						}

						if (jsonObject.has("publishedAt")) {
							currItem.setPublishedAt(jsonObject.getString("publishedAt"));
						}
						if (jsonObject.has("sortPublishAt")) {
							currItem.setSortPublishAt(jsonObject.getString("sortPublishAt"));
						}
						feedList.add(currItem);
					}
					// }
				}
				return feedList;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private void setImageAndDescription(String content) throws IOException, BadLocationException {
		Document htmlDoc = Jsoup.parse(content);
		Elements contentDetails = htmlDoc.getElementsByTag("p");
		String img = "", desc = "";
		for (org.jsoup.nodes.Element element : contentDetails) {
			if (element.getElementsByTag("img").attr("src").length() > 0)
				img += element.getElementsByTag("img").attr("src").toString();
			else
				news.setImage("");
			if (!element.getElementsByTag("p").equals(""))
				desc += element.getElementsByTag("p").text().toString();
		}
		news.setDescription(desc);
		news.setImage(img);
	}

}
