package utils;

public interface Config {
	static final String ENVIRONMENT = "PRODUCTION";//DEVELOPMENT|PRODUCTION
    static final String FIREBASE_URL_DEVELOPMENT="https://testnews-616cd.firebaseio.com";
    static final String FIREBASE_URL_PRODUCTION="https://apna-jalaun.firebaseio.com";
    static final String FIREBASE_URL=ENVIRONMENT.equals("PRODUCTION")?FIREBASE_URL_PRODUCTION:FIREBASE_URL_DEVELOPMENT;

    
	static final String BASE_URL_JAGRAN = "http://www.jagran.com";
	static final String PAGE_URL_JAGRAN = "/local/uttar-pradesh_jalaun-news-hindi-page";
	static final String BASE_URL_UJALA = "http://www.amarujala.com/uttar-pradesh/jalaun";
	static final String BASE_URL_JALAUN_TIMES = "https://public-api.wordpress.com/rest/v1.1/sites/jalauntimessite.wordpress.com/posts?number=50";
	static final String AD_END_POINT = "/list/ads.json";
	static final String LOCAL_NEWS_END_POINT = "/list/news.json";
	static final String FEEDS_END_POINT = "feeds";
	static final int MAXIMUM_PAGE_TO_BE_FETCHED = 3;
}