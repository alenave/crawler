package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Operations {
	public static String dateTimeFormatChangeWithT(String time) {
		SimpleDateFormat withT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date d = null;
		try {
				d = withT.parse(time);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return output.format(d);
	}

	public static String dateTimeFormatChangeWithDayName(String time) {
		SimpleDateFormat dayName = new SimpleDateFormat("E, dd MMM yyyy hh:mm a Z");
		SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date d = null;
		try {
			if (time.contains(","))
				d = dayName.parse(time);
			else
				return time;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return output.format(d);
	}
}
